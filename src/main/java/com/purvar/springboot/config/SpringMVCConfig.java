package com.purvar.springboot.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class SpringMVCConfig extends WebMvcConfigurerAdapter {
	@Override
	public void addViewControllers(ViewControllerRegistry registry){
	//super.addViewControllers(registry);
	//浏览器发送/请求来到index
	registry.addViewController("/").setViewName("login");
	registry.addViewController("/index").setViewName("login");
	registry.addViewController("/login").setViewName("login");
	}
}
